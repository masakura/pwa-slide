const CACHE_NAME = 'pwa-slide-cache';
const base = location.toString().replace('/sw.js', '');
const urlsToCache = [
  'https://fonts.googleapis.com/css?family=Montserrat:700',
  'https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic',
  'https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gnD_g.woff2',
  'https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0Zdc0.woff2',
  'https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0b.woff2',
  'https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOUuhp.woff2',
  '/index.html',
  '/lib/revealjs/night.min.css',
  '/lib/revealjs/reveal.min.css',
  '/lib/revealjs/reveal.min.js',
  '/lib/revealjs/lib/css/zenburn.min.css',
  '/lib/revealjs/lib/js/head.min.js',
  '/lib/revealjs/plugin/highlight/highlight.min.js',
  '/assets/grant-notify.png',
].map(p => p[0] === '/' ? `${base}${p}` : p);

self.addEventListener('install', event => {
  event.waitUntil(caches.open(CACHE_NAME)
    .then(cache => {
      console.log('Opened cache');
      return cache.addAll(urlsToCache)
    })
    .catch(error => {
      console.log(error);
    }));
});

self.addEventListener('fetch', event => {
  const request = event.request;

  if (!urlsToCache.includes(request.url)) return;

  const process = Promise.resolve()
    .then(() => {
      if (navigator.onLine) {
        return fetch(request)
          .then(response => {
            if (!response) return response;

            return caches.open(CACHE_NAME)
              .then(cache => cache.put(request, response.clone()))
              .then(() => response);
          });
      }

      return caches.open(CACHE_NAME).then(cache => cache.match(request));
    });

  event.respondWith(process);
});
