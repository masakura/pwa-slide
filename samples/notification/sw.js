const notify = message => {
  self.registration.showNotification(message.title, message);
};

self.addEventListener('message', event => {
  const message = JSON.parse(event.data);
  console.log(message);

  if (message.delay) {
    setTimeout(() => notify(message), message.delay);
  } else {
    notify(message);
  }
});
