self.addEventListener('fetch', event => {
  const invoke = async (request) => {
    console.log(request.url);
    try {
      // 普通に処理する
      const response = await fetch(request);

      // レスポンスをキャッシュに入れる
      if (response) {
        const cache = await caches.open('CACHE-APP');
        cache.put(request, response.clone());
        return response;
      }

    } catch {
      // エラーの場合 (オフラインなど) キャッシュから返す
      return await caches.match(request);
    }
  };

  event.respondWith(invoke(event.request));
});
