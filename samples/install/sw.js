const CACHE_NAME = 'install-app';

// キャッシュするファイルのリストを作成
const base = location.toString().replace('/sw.js', '');
const urlsToCache = [
  'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/dark.min.css',
  'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/json.min.js',
  '/index.html',
  '/android-18.png',
].map(p => p[0] === '/' ? `${base}${p}` : p);
console.log(urlsToCache);

// Service Worker のインストール時に呼び出される
self.addEventListener('install', event => {
  const invoke = async () => {
    // キャッシュを開いて、アプリに必要なファイルを読み込んでキャッシュする
    const cache = await caches.open(CACHE_NAME);
    cache.addAll(urlsToCache);
  };

  event.waitUntil(invoke());
});

self.addEventListener('fetch', event => {
  const invoke = async (request) => {
    // キャッシュから取得する
    const response = caches.match(request);
    if (response) return response;

    // キャッシュにないものは普通に処理する
    return await fetch(request);
  };

  event.respondWith(invoke(event.request));
});
