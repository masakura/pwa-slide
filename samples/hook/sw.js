// リクエストのたびにこのイベントが呼び出される
self.addEventListener('fetch', event => {
  const invoke = async (request) => {
    try {
      // いつも通りリクエストを処理する
      return await fetch(request);
    } catch {
      // リクエストの処理に失敗したとき (オフラインとか)
      return new Response('It \'s offline!');
    }
  };

  event.respondWith(invoke(event.request));
});
