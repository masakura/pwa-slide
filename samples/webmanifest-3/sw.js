const CACHE_NAME = 'banner-cache';
const base = location.toString().replace('/sw.js', '');
const urlsToCache = [
  'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/dark.min.css',
  'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/json.min.js',
  '/index.html',
].map(p => p[0] === '/' ? `${base}${p}` : p);
console.log(urlsToCache);

self.addEventListener('install', event => {
  const invoke = async () => {
    const cache = await caches.open(CACHE_NAME);
    await cache.addAll(urlsToCache);
  };

  event.waitUntil(invoke());
});

self.addEventListener('fetch', event => {
  const addToCache = async (request) => {
    const response = await fetch(request);
    const cache = await caches.open(CACHE_NAME);
    cache.put(request, response.clone());
    return response;
  };
  const invoke = async (request) => {
    const response = await caches.match(request);
    return response || await addToCache(request);
  };

  event.respondWith(invoke(event.request));
});
