class Storage {
  constructor() {
    const request = indexedDB.open('data', 100);
    request.addEventListener('upgradeneeded', event => {
      const db = event.target.result;
      db.createObjectStore('state', {keyPath: 'id'});
    });
    request.addEventListener('success', event => {
      const db = event.target.result;
      db.close();
    });
  }

  update(state) {
    const request = indexedDB.open('data');

    return new Promise(resolve => {
      request.addEventListener('success', event => {
        const db = event.target.result;

        const transaction = db.transaction('state', 'readwrite');
        const store = transaction.objectStore('state');
        const result = store.put({id: 1, state});

        result.addEventListener('success', () => {
          resolve();
        });
      });
    });
  }

  get() {
    const request = indexedDB.open('data');

    return new Promise(resolve => {
      request.addEventListener('success', event => {
        const db = event.target.result;

        const transaction = db.transaction('state', 'readonly');
        const store = transaction.objectStore('state');
        const result = store.get(1);

        result.addEventListener('success', event => {
          const result = event.target.result;
          resolve(result ? result.state : 'pending');
        });
      });
    });
  }
}
