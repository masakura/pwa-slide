class Client {
  constructor(base = 'https://pwa-push.azurewebsites.net') {
    this.base = base;
  }

  async publicKey() {
    const response = await fetch(`${this.base}/keys/public`);
    const key = (await response.json()).publicKey;
    return this.toBinary(key);
  }

  async echo(subscription) {
    const message = Object.assign({
      data: {
        url: window.location.toString(),
      }
    }, JSON.parse(JSON.stringify(subscription)));
    await fetch(`${this.base}/echo`, {
      method: 'POST',
      body: JSON.stringify(message),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    });
  }

  toBinary(base64) {
    const data = base64.replace(/-/g, '+').replace(/_/g, '/');
    const raw = window.atob(data);
    const binary = new Uint8Array(raw.length);
    for (let i = 0; i < raw.length; i++) {
      binary[i] = raw.charCodeAt(i);
    }
    return binary;
  }
}

class Push {
  constructor(subscription) {
    this.subscription = subscription;
    this.client = new Client();
  }

  echo() {
    this.client.echo(this.subscription);
  }
}

class PushManager {
  constructor(registration) {
    this.pushManager = registration.pushManager;
    this.client = new Client();
  }

  async subscribe() {
    const subscription = (await this.getRegistration()) || (await this.register());
    return new Push(subscription);
  }

  async getRegistration() {
    return await this.pushManager.getSubscription();
  }

  async register() {
    const key = await this.client.publicKey();
    const subscription = await this.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: key,
    });
    return subscription;
  }
}

class PushReceiver {
  constructor($receiver) {
    this.$receiver = $receiver;
    this.storage = new Storage();
  }

  async toWait() {
    await this.storage.update('waiting');
    await this.refresh();
  }

  async refresh() {
    const state = await this.storage.get();
    this.$receiver.innerHTML = this.get(state);
  }

  get(state) {
    switch (state) {
      case 'waiting': return '待機中...';
      case 'received': return '受け取りました!';
      default: return 'ボタンをクリック!';
    }
  }
}

(async () => {
  const registration = await navigator.serviceWorker.register('sw.js');

  const pushManager = new PushManager(registration);
  const push = await pushManager.subscribe();
  const receiver = new PushReceiver(document.querySelector('#receiver'));

  document.querySelector('#echo')
    .addEventListener('click', () => {
      push.echo();
      receiver.toWait();
    });

  navigator.serviceWorker.addEventListener('message', async () => receiver.refresh());

  await receiver.refresh();
})();
