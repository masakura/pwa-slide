importScripts('./storage.js');

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

// PUSH が発生すると push イベントが呼び出される
self.addEventListener('push', event => {
  // データを取り出す
  const message = event.data.json();

  const invoke = async () => {
    await self.registration.showNotification(message.title, message)

    const storage = new Storage();
    await storage.update('received');

    const result = await self.clients.matchAll({});
    result.forEach(client => client.postMessage('update'));
  }

  // 通知する
  event.waitUntil(invoke());
});

self.addEventListener('notificationclick', event => {
  const invoke = async (notification) => {
    notification.close();

    const result = await self.clients.matchAll({});
    if (result.length <= 0) {
      const client = await self.clients.openWindow(notification.data.url);
      client.postMessage('update');
    } else {
      result.forEach(client => client.postMessage('update'));
    }
  };

  event.waitUntil(invoke(event.notification));
}, false);
