const heavy = (time = 3000) => {
  const start = Date.now();
  console.log('Start!');

  while (true) {
    if ((Date.now() - start) > time) break;
  }

  console.log('Finish');
};

const verbose = ($button, action) => {
  const $verbose = $button.querySelector('.verbose');
  const start = Date.now();
  const updateVerbose = () => {
    const delta = Date.now() - start;
    $verbose.innerHTML = `(${delta} ms)`;
  };

  const run = async () => {
    const id = setInterval(updateVerbose, 100);

    await action() || Promise.resolve();

    clearInterval(id);
    updateVerbose();
  };

  setTimeout(updateVerbose, 3);
  setTimeout(run, 3);
};
